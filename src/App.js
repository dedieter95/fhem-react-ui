import React from 'react'
import './App.css'
import MainPage from './app/components/MainPage'
import Header from './app/components/Header'
import NavBar from './app/components/NavBar'
import About from './app/components/About'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import CssBaseline from '@material-ui/core/CssBaseline'
import { createStore, applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga'
import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit'
import rootReducer from './reducers'
import { devicesSliceReducer } from './reducers/slice'
import { all } from 'redux-saga/effects'
import { Provider } from 'react-redux'
import logger from 'redux-logger'

function * rootSaga () {
  yield all([])
}

const devMode = process.env.NODE_ENV === 'development'
//create Saga Middleware
const sagaMiddleware = createSagaMiddleware()

const middleware = [...getDefaultMiddleware({ thunk: false }), sagaMiddleware]

if (devMode) {
  middleware.push(logger)
}

function App () {
  const store = configureStore({
    reducer: { deviceList: devicesSliceReducer },
    middleware
  })
  sagaMiddleware.run(rootSaga)

  return (
    <div className='App'>
      <CssBaseline />
      <Provider store={store}>
        <Router>
          <Header />
          <div
            style={{ display: 'flex', flexDirection: 'row', height: '100vh' }}
          >
            <NavBar />
            <Switch>
              <Route path='/home' component={MainPage} />
              <Route path='/about' component={About} />
              <Route path='/' component={MainPage} />
            </Switch>
          </div>
        </Router>
      </Provider>
    </div>
  )
}

export default App
