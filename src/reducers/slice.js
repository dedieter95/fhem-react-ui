import { createSlice } from '@reduxjs/toolkit'

const devicesSlice = createSlice({
  name: 'deviceList',
  initialState: { devices: {} },
  reducers: {
    create: {
      reducer (state, action) {
        const deviceName = action.payload.deviceName
        console.log('deviceName')
        state.devices[deviceName] = {
          name: deviceName
        }
      },
      prepare (deviceName) {
        return { payload: { deviceName } }
      }
    },

    update: {
      reducer (state, action) {
        const valueName = action.payload.valueName
        const device = action.payload.device
        const value = action.payload.value
        state.devices[device][valueName] = value
      },
      prepare (device, valueName, value) {
        return { payload: { device, valueName, value } }
      }
    }
  }
})

export const devicesSliceAction = devicesSlice.actions

export const devicesSliceReducer = devicesSlice.reducer
