import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import axios from 'axios'

export const readingsVal = async (device, reading) => {
  const cmd = `cmd={ReadingsVal("${device}","${reading}",'')}`
  let response
  try {
    response = await axiosClient(cmd)
  } catch (err) {
    // Handle Error Here
    console.error(err)
  }

  return response.data
}

//http://192.168.1.8:8083/fhem/?cmd=jsonlist2+BR_4FC240_Clima%2CBrithday_Fynn+ValvePosition+controlMode+batteryLevel+DaysSinceBirth&fwcsrf=&XHR=1&_=1605521138152
export const jsonList2 = async (deviceList, readings) => {
  let cmd = `cmd=jsonList2+`
  let deviceUrlPart = ''
  let readingUrlPart = ''

  for (const device of deviceList) {
    if (deviceUrlPart !== '') deviceUrlPart += '%2C'
    deviceUrlPart += device
  }

  for (const reading of readings) {
    readingUrlPart += '+' + reading
  }

  cmd = cmd + deviceUrlPart + readingUrlPart

  let response
  try {
    response = await axiosClient(cmd)
  } catch (err) {
    // Handle Error Here
    console.error(err)
  }

  return response.data
}

jsonList2.propTypes = {
  deviceList: PropTypes.array,
  readings: PropTypes.array
}

//http://192.168.1.8:8083/fhem/?cmd=set+BR_4FC240_Clima+desired-temp+13&fwcsrf=&XHR=1&_=1605529173144
export const setReadingsVal = async (device, reading, value) => {
  const cmd = `cmd=set+${device}+${reading}+${value}`
  let response
  try {
    response = await axiosClient(cmd)
  } catch (err) {
    // Handle Error Here
    console.error(err)
  }

  return response.data
}

async function axiosClient (cmd) {
  console.log('axiosClient', cmd)
  const urlBase = 'http://192.168.1.8:8083/fhem/?'
  const fwcsrf = 'fwcsrf=&XHR=1&_=1605521138152'

  const url = `${urlBase}${cmd}&${fwcsrf}`
  // switch (cmd) {
  //   case 'ReadingsVal':
  //     url = `${urlBase}/cmd={ReadingsVal("${deviceUrlPart}","${reading}",'')}&${fwcsrf}`
  //     break
  //   case 'jsonlist2': {
  //     url = `${urlBase}/cmd=jsonlist2+${deviceUrlPart}&${fwcsrf}`
  //     break
  //   }
  //   default:
  //     break
  // }

  const headers = {
    headers: {
      //'content-type': 'application/x-www-form-urlencoded;charset=utf-8'
    }
  }

  console.log(url)
  let returnValues = ''
  //
  const response = await axios.get(url)

  return response
}

axiosClient.propTypes = {
  cmd: PropTypes.string
}
