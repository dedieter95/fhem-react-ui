import React, { useState, useEffect } from 'react'
import CircularSlider from 'react-circular-slider-svg'
import { readingsVal, setReadingsVal } from '../backend'
import { makeStyles } from '@material-ui/core/styles'
import { devicesSliceAction } from '../../reducers/slice'
import { connect } from 'react-redux'

const useStyles = makeStyles({
  desiredTemp: {
    position: 'absolute',
    top: props => (props.size ? props.size * 0.44 : '50%'),
    left: props => (props.size ? props.size / 2 : '50%'),
    transform: 'translate(-50%, -50%)',
    zIndex: '1',
    userSelect: 'none'
  },
  measuredTemp: {
    position: 'absolute',
    top: props => (props.size >= 150 ? '40px' : '20%'),
    left: props => (props.size ? props.size / 2 : '50%'),
    transform: 'translate(-50%, -50%)',
    zIndex: '1',
    userSelect: 'none',
    fontSize: '10px'
  },
  valvePosition: {
    position: 'absolute',
    top: props => (props.size ? props.size * 0.65 : '65%'),
    left: props => (props.size ? props.size / 2 : '50%'),
    transform: 'translate(-50%, -50%)',
    zIndex: '1',
    userSelect: 'none'
  }
})

const readings = [
  'measured-temp',
  'ValvePosition',
  'desired-temp',
  'controlMode'
]

function BaseThermostat (props) {
  const { deviceName, size, update, create } = props

  const classes = useStyles(props)

  const defaultSize = 200
  const sizeValue = size || defaultSize

  const [valvePosition, setValvePosition] = useState(0)
  const [desiredTemp, setDesiredTemp] = useState(0)
  const [desiredTempEnd, setDesiredTempEnd] = useState(0)
  const [measuredTemp, setMeasuredTemp] = useState(0)

  useEffect(() => {
    async function fetchData () {
      // You can await here
      setValvePosition(await readingsVal(deviceName, 'ValvePosition'))
      setDesiredTemp(await readingsVal(deviceName, 'desired-temp'))
      setMeasuredTemp(await readingsVal(deviceName, 'measured-temp'))
    }
    const value = fetchData()
    return () => {}
  }, [deviceName])

  return (
    <div
      style={{
        height: `${size}px`,
        width: `${size}px`,
        position: 'relative',
        display: 'flex',
        justifyContent: 'center',
        left: '50%',
        transform: 'translateX(-50%)'
      }}
    >
      <CircularSlider
        size={sizeValue}
        handle1={{
          value: desiredTemp,
          onChange: temp => {
            const t = Math.round(temp / 0.5) * 0.5
            setDesiredTemp(t)
          }
        }}
        onControlFinished={
          () => update(deviceName, 'desired-temp', desiredTemp)
          //   setReadingsVal(device, 'desired-temp', desiredTemp)
        }
        arcColor='#690'
        startAngle={70}
        endAngle={290}
        minValue={0}
        maxValue={30}
      />
      <div className={classes.measuredTemp}>{measuredTemp}°C</div>
      <div className={classes.desiredTemp}>{desiredTemp}°C</div>
      <div className={classes.valvePosition}>{valvePosition}%</div>
    </div>
  )
}

export const Thermostat = connect(
  (state, ownProps) => {
    const { deviceName, size } = ownProps

    const devices = state.deviceList.devices

    return { deviceName: deviceName, size: size, deviceSate: 'deviceSate' }
  },
  {
    update: (deviceName, valueName, desiredTemp) =>
      devicesSliceAction.update(deviceName, valueName, desiredTemp),
    create: deviceName => devicesSliceAction.create(deviceName)
  }
)(BaseThermostat)
