import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import logo from '../../images/smart-home.png'
import Clock from 'react-live-clock'

const useStyles = makeStyles({
  header: {
    background: '#131921',
    display: 'flex',
    borderBottom: '1px solid black',
    color: 'white',
    height: '70px',
    padding: '10px 10px',
    marginBottom: '10px',
    objectFit: 'contain ',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  img: {
    width: '70px'
  },
  timer: {
    alignSelf: 'center',
    fontSize: '30px',
    fontWeight: 'bold'
  },
  space: {
    flexGrow: '1'
  }
})

function Header () {
  const classes = useStyles()

  return (
    <div className={classes.header}>
      <img className={classes.img} src={logo} alt='' />
      <div className={classes.space}></div>
      <div className={classes.timer}>
        <Clock
          format={'HH:mm:ss'}
          style={{ fontSize: '30px' }}
          ticking={true}
          timezone={'Europe/Berlin'}
        />
      </div>
    </div>
  )
}

export default Header
