import React, { useEffect, useState } from 'react'
import { readingsVal } from '../backend/axiosClient'

function Label (props) {
  const { name, deviceName, deviceReading } = props

  const [data, setData] = useState()

  useEffect(() => {
    //const value = await readingsVal(device, deviceReading)
    async function fetchData () {
      // You can await here
      const response = await readingsVal(deviceName, deviceReading)
      setData(response)
      // ...
    }
    const value = fetchData()

    return () => {}
  }, [])

  return <div>{data}</div>
}

export default Label
