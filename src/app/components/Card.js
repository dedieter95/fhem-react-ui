import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Paper from '@material-ui/core/Paper'
import Card from '@material-ui/core/Card'
import CardHeader from '@material-ui/core/CardHeader'
import CardContent from '@material-ui/core/CardContent'
import Divider from '@material-ui/core/Divider'

const useStyles = makeStyles({
  card: {
    backgroundColor: '#F5F5F5',
    height: props => (props.height ? props.height : '200px'),
    width: props => props.width || '100%',
    margin: '10px'
  }
})

function CardContainer (props) {
  const { title, subtitle, children } = props
  const classes = useStyles(props)
  return (
    <Card className={classes.card}>
      <CardHeader title={title} subheader={subtitle} />
      <Divider />
      <CardContent>{children}</CardContent>
    </Card>
  )
}

export default CardContainer
