import React from 'react'
import Label from './Label'
import CardContainer from './Card'
import { Thermostat } from '../widgets/Thermostat'
import { devicesSliceAction } from '../../reducers/slice'
import { connect } from 'react-redux'
import { jsonList2 } from '../backend'

function getAllTypes (children) {
  let deviceList = []
  let readingList = []

  let test = React.Children.map(children, children => {
    let devices = []
    let readings = []
    if (!children.props) return

    if (children.props.deviceName) devices.push(children.props.deviceName)

    if (children.props.deviceReading)
      readings.push(children.props.deviceReading)

    if (children.props.children) {
      let response = getAllTypes(children.props.children)
      if (response.readings) readings = readings.concat(response.readings)
      if (response.devices) devices = devices.concat(response.devices)
    }

    return { devices: devices, readings: readings }
  })

  for (let i = 0; i < test.length; i++) {
    deviceList = deviceList.concat(test[i].devices)
    readingList = readingList.concat(test[i].readings)
  }
  let types = { devices: deviceList, readings: readingList }

  return types
}

async function fetchDeviceData (uniqueDevices, uniqueReadings) {
  const response = await jsonList2(uniqueDevices, uniqueReadings)
  return response
}

async function BaseComponent (props) {
  const {
    type,
    devices,
    create,
    deviceName,
    children,
    ...remainingProps
  } = props

  console.log('react child')
  const deviceinfo = getAllTypes(children)
  let uniqueDevices = [...new Set(deviceinfo.devices)]
  let uniqueReadings = [...new Set(deviceinfo.readings)]

  uniqueDevices.forEach(deviceName => {
    if (!(deviceName in devices)) {
      create(deviceName)
    }
  })

  const resp = await fetchDeviceData(uniqueDevices, uniqueReadings)
  console.log(resp)
  const types = {
    Label,
    Thermostat,
    CardContainer
  }

  const Type = types[type]
  return <Type deviceName={deviceName} {...remainingProps}></Type>
}

export const Component = connect(
  state => {
    const devices = state.deviceList.devices
    return { devices: devices }
  },
  {
    update: (deviceName, valueName, desiredTemp) =>
      devicesSliceAction.update(deviceName, valueName, desiredTemp),
    create: deviceName => devicesSliceAction.create(deviceName)
  }
)(BaseComponent)
