import React, { useState, useEffect } from 'react'
import { Component } from './BaseComponent'
import CardContainer from './Card'
import Label from './Label'
import { Thermostat } from '../widgets/Thermostat'
function MainPage (props) {
  return (
    <Component>
      <CardContainer
        title='Thermostat'
        subtitle='Desired temperature'
        width='300px'
        height='250px'
        type='CardContainer'
      >
        <Thermostat type='Thermostat' deviceName='BR_4FC240_Clima' />
      </CardContainer>
      <CardContainer
        title='Clima'
        subtitle='desired-temp'
        height='250px'
        width='200px'
        type='CardContainer'
      >
        <Thermostat type='Thermostat' deviceName='BR_4FC240_Clima' size={150} />
        <Label
          deviceName='Brithday_Fynn'
          deviceReading='Birthday'
          type='Label'
        ></Label>
      </CardContainer>
      <CardContainer
        title='Clima'
        subtitle='desired-temp'
        height='250px'
        width='200px'
      >
        <div>Birthday</div>
        <Label
          deviceName='Brithday_Fynn'
          deviceReading='Birthday'
          type='Label'
        ></Label>
      </CardContainer>
    </Component>
  )
}

export default MainPage
