import React from 'react'
import { NavLink } from 'react-router-dom'
import { makeStyles } from '@material-ui/core/styles'
import HomeOutlinedIcon from '@material-ui/icons/HomeOutlined'
import InfoOutlinedIcon from '@material-ui/icons/InfoOutlined'
import BathtubOutlinedIcon from '@material-ui/icons/BathtubOutlined'
import styled from 'styled-components'

const useStyles = makeStyles({
  navBar: {
    display: 'flex',
    flexDirection: 'column',
    borderRight: '1px solid black',
    height: '100%',
    width: '150px',
    margin: '0px 10px 0px 0px',
    padding: '0px 0px',
    objectFit: 'contain ',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    backgroundColor: '#F3F3F3'
  },
  text: {
    marginLeft: '10px'
  },
  icon: {
    width: '30px',
    height: '30px',
    paddingLeft: '5px'
  },
  selectedNav: {
    color: 'green',
    backgroundColor: '#00aeec'
  }
})

const StyledLink = styled(NavLink)`
  text-decoration: none;
  color: black;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: flex-start;
  font-size: 15px;
  padding-bottom: 5px;
  padding-top: 5px;

  &:hover {
    font-weight: bold;
    color: #00aeec;
    background-color: white;
  }
  &.${props => props.activeClassName} {
    color: white;
    background-color: #00abe9;
  }
`

function NavBar () {
  const classes = useStyles()

  return (
    <div className={classes.navBar}>
      <StyledLink to='/home' activeClassName='selectedNav'>
        <HomeOutlinedIcon className={classes.icon} />
        <div className={classes.text}>Home</div>
      </StyledLink>
      <StyledLink to='/about' activeClassName='selectedNav'>
        <InfoOutlinedIcon className={classes.icon} />
        <div className={classes.text}>About Us</div>
      </StyledLink>
      <StyledLink to='/bathroom' activeClassName='selectedNav'>
        <BathtubOutlinedIcon className={classes.icon} />
        <div className={classes.text}>Bathroom</div>
      </StyledLink>
      <StyledLink to='/shop' activeClassName='selectedNav'></StyledLink>
    </div>
  )
}

export default NavBar
